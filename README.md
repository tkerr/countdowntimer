# aunit #
Arduino Countdown Timer

Countdown timer module used for managing event timeouts.  
Immune to hardware timer rollover.

### Documentation ###
Documentation is contained in the CountdownTimer.h header file.

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License
https://opensource.org/licenses/MIT
