/******************************************************************************
 * CountdownTest.ino
 * Copyright (c) 2019 Tom Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Unit test sketch for the Arduino CountdownTimer class.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "CountdownTimer.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/

#define ASSERT_TRUE(A)  logResult(#A, (A == true));
#define ASSERT_FALSE(A) logResult(#A, (A == false));


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * logMessage
 **************************************/ 
void logMessage(const char* msg)
{
    Serial.print(millis());
    Serial.print(',');
    Serial.println(msg);
}

/**************************************
 * logResult
 **************************************/ 
void logResult(const char* test, bool pass)
{
    Serial.print(millis());
    Serial.print(',');
    Serial.print(test);
    if (pass)
    {
        Serial.println(" Passed.");
    }
    else
    {
        Serial.println(" Failed.");
    }
}

/**************************************
 * test1
 **************************************/ 
void test1()
{
    // A CountdownTimer should default construct to expired.
    logMessage("Test 1");
    CountdownTimer timer;
    ASSERT_TRUE(timer.isExpired())
}
 
/**************************************
 * test2
 **************************************/ 
void test2()
{
    // A default CountdownTimer should immediately expire.
    logMessage("Test 2");
    CountdownTimer timer;
    timer.start();
    ASSERT_TRUE(timer.isExpired())
}

/**************************************
 * test3
 **************************************/ 
void test3()
{
    // A default CountdownTimer can have its timeout period set.
    logMessage("Test 3");
    CountdownTimer timer;
    uint32_t start_time = timer.start(50);
    ASSERT_FALSE(timer.isExpired())
    delay(52);
    ASSERT_TRUE(timer.isExpired())
    ASSERT_TRUE(start_time > 0)
}

/**************************************
 * test4
 **************************************/ 
void test4()
{
    // Set a timer to expired before it times out.
    logMessage("Test 4");
    CountdownTimer timer;
    timer.start(50);
    ASSERT_FALSE(timer.isExpired())
    delay(10);
    ASSERT_FALSE(timer.isExpired())
    timer.setExpired();
    ASSERT_TRUE(timer.isExpired())
}

/**************************************
 * test5
 **************************************/ 
void test5()
{
    // Construct a timer with a specified timeout.
    logMessage("Test 5");
    CountdownTimer timer(50);
    ASSERT_FALSE(timer.isExpired())
    delay(10);
    ASSERT_FALSE(timer.isExpired())
    delay(42);
    ASSERT_TRUE(timer.isExpired())
}

/**************************************
 * test6
 **************************************/ 
void test6()
{
    // A timer constructed with a specified timeout can be restarted.
    logMessage("Test 6");
    CountdownTimer timer(50);
    ASSERT_FALSE(timer.isExpired())
    delay(10);
    ASSERT_FALSE(timer.isExpired())
    delay(42);
    ASSERT_TRUE(timer.isExpired())
    timer.start();
    ASSERT_FALSE(timer.isExpired())
    delay(10);
    ASSERT_FALSE(timer.isExpired())
    delay(42);
    ASSERT_TRUE(timer.isExpired())
} 

/**************************************
 * test7
 **************************************/ 
void test7()
{
    // A timer constructed with a specified timeout 
    // can be restarted with a new timeout value.
    logMessage("Test 7");
    CountdownTimer timer(50);
    ASSERT_FALSE(timer.isExpired())
    delay(10);
    ASSERT_FALSE(timer.isExpired())
    delay(42);
    ASSERT_TRUE(timer.isExpired())
    timer.start(75);
    ASSERT_FALSE(timer.isExpired())
    delay(50);
    ASSERT_FALSE(timer.isExpired())
    delay(27);
    ASSERT_TRUE(timer.isExpired())
} 

/**************************************
 * test8
 **************************************/ 
void test8()
{
    // Simulate hardware timer rollover.
    logMessage("Test 8");
    CountdownTimer timer;
    ASSERT_TRUE(timer.isExpired())
    
    // Simulate 100 milliseconds from hardware timer rollover.
    timer.setOffset((0xFFFFFFFF - 100) - millis());
    ASSERT_TRUE(timer.isExpired())
    
    timer.start(200);
    ASSERT_FALSE(timer.isExpired())
    delay(80);
    ASSERT_FALSE(timer.isExpired())
    delay(25);
    
    // Elapsed time = 105ms
    // Timer rollover occured during the previous delay.
    ASSERT_FALSE(timer.isExpired())
    
    delay(90);
    ASSERT_FALSE(timer.isExpired())
    delay(10);
    ASSERT_TRUE(timer.isExpired())
}

/**************************************
 * test9
 **************************************/ 
void test9()
{
    // Simulate typical use.
    logMessage("Test 9");
    uint32_t timeout = 50;
    CountdownTimer timer;
    uint32_t start_ms = millis();
    
    // Typical use:
    // Start a timer, do some chores, check for expiration.
    timer.start(timeout);
    while (!timer.isExpired()) {}
    
    uint32_t elapsed_ms = millis() - start_ms;
    ASSERT_TRUE((elapsed_ms >= timeout))
    ASSERT_TRUE((elapsed_ms < (timeout + 3)))
} 

/**************************************
 * setup
 **************************************/ 
void setup()
{       
    // Serial port initialization.
    Serial.begin(9600);
    delay(2000);
    logMessage("CountdownTimer test");

    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    test9();
}


/**************************************
 * loop
 **************************************/ 
void loop()
{

}


/******************************************************************************
 * Private functions.
 ******************************************************************************/


// End of file.