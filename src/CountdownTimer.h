/******************************************************************************
 * CountdownTimer.h
 * Copyright (c) 2019 Tom Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Arduino countdown timer module used for managing event timeouts. 
 * Designed to be immune to hardware timer rollover.
 */

#ifndef _COUNTDOWN_TIMER_H
#define _COUNTDOWN_TIMER_H

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdint.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/

 
/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/
 
/**
 * @brief
 * Countdown timer class designed for managing event timeouts.
 */
class CountdownTimer
{
public:

    /**
     * @brief
     * Default constructor.
     *
     * Timer object is constructed and is initially expired.
     */
    CountdownTimer();
    
    /**
     * @brief
     * Construct a timer object and start counting down.
     *
     * @param timeout Expiration time in milliseconds.
     */
    CountdownTimer(uint32_t timeout);
    
    /**
     * @brief
     * Determine if timer has expired.
     *
     * @return True if timer has expired, false otherwise.
     */
    bool isExpired();
    
    /**
     * @brief
     * Stop countdown timing and force the timer into the expired state.
     */
    void setExpired();
    
    /**
     * @brief
     * Add an offset to the system time for testing purposes.
     *
     * THIS IS USED FOR TEST PURPOSES ONLY.  It adds an offset to the
     * millis() value.  Intended to be used only to test simulated system 
     * clock rollover.  Not for operational use.
     *
     * @param offset_ms System time offset in milliseconds.
     */
    void setOffset(uint32_t offset_ms) {m_offset_ms = offset_ms;};
    
    /**
     * @brief
     * Start or restart a countdown timer operation.
     *
     * Initiates a countdown sequence on an existing timer.
     * If timeout parameter is not supplied (or set to zero), then the
     * existing timeout value is used.
     *
     * @param timeout Expiration time in milliseconds.
     *
     * @return The system timer value when the timer was started.
     */
    uint32_t start(uint32_t timeout=0);

private:
    
    uint32_t systemTimeMs();  //!< Return system time in milliseconds
    
    uint32_t m_timeout_ms;    //!< Timeout expiration in milliseconds
    uint32_t m_update_ms;     //!< System timer value at last update
    uint32_t m_remain_ms;     //!< Remaining time left before expiration
    uint32_t m_offset_ms;     //!< System time offset used only for testing
};


#endif // _COUNTDOWN_TIMER_H
