/******************************************************************************
 * CountdownTimer.cpp
 * Copyright (c) 2019 Tom Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Arduino countdown timer module used for managing event timeouts. 
 * Designed to be immune to hardware timer rollover.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "CountdownTimer.h"


/******************************************************************************
 * Global parameters.
 ******************************************************************************/
 
 
/******************************************************************************
 * Forward references.
 ******************************************************************************/
 

/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions and methods.
 ******************************************************************************/

/**************************************
 * CountdownTimer::CountdownTimer()
 **************************************/ 
CountdownTimer::CountdownTimer() : 
    m_timeout_ms(0),
    m_update_ms(0),
    m_remain_ms(0),
    m_offset_ms(0)
{

}


/**************************************
 * CountdownTimer::CountdownTimer()
 **************************************/ 
CountdownTimer::CountdownTimer(uint32_t timeout) : 
    m_timeout_ms(timeout),
    m_remain_ms(timeout),
    m_offset_ms(0)
{
    m_update_ms = systemTimeMs();
}


/**************************************
 * CountdownTimer::isExpired()
 **************************************/ 
bool CountdownTimer::isExpired()
{
    // Zero time remaining signifies an expired timer.
    if (m_remain_ms == 0) return true;
    
    // Handle system timer rollover.
    uint32_t now = systemTimeMs();
    uint32_t elapsed;
    if (now < m_update_ms)
    {
        elapsed = now + 1 + (0xFFFFFFFF - m_update_ms);
    }
    else
    {
        elapsed = now - m_update_ms;
    }
    
    // Determine if timer has expired.
    bool expired = false;
    if (elapsed >= m_remain_ms)
    {
        m_remain_ms = 0;
        expired = true;
    }
    else
    {
        m_remain_ms -= elapsed;
    }
    
    m_update_ms = now;
    return expired;
}


/**************************************
 * CountdownTimer::setExpired()
 **************************************/ 
void CountdownTimer::setExpired()
{
    m_remain_ms = 0;
}


/**************************************
 * CountdownTimer::start()
 **************************************/ 
uint32_t CountdownTimer::start(uint32_t timeout)
{
    if (timeout > 0)
    {
        m_timeout_ms = timeout;
    }
    m_remain_ms = m_timeout_ms;
    m_update_ms = systemTimeMs();
    return m_update_ms;
}


/******************************************************************************
 * Private functions and methods.
 ******************************************************************************/
 
/**************************************
 * CountdownTimer::systemTimeMs()
 **************************************/  
uint32_t CountdownTimer::systemTimeMs()
{
    return millis() + m_offset_ms;
}
    
    
/******************************************************************************
 * Interrupt service routines.
 ******************************************************************************/

 
// End of file.
